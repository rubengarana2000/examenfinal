<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actionConsulta1() {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT p.provincia 
                FROM provincias p 
                WHERE p.poblacion=(SELECT MIN(c2.poblacion) 
                FROM (SELECT p.provincia,p.poblacion FROM provincias p 
                WHERE p.provincia IN (SELECT p.provincia 
                FROM provincias p 
                WHERE p.autonomia=
                (SELECT p.autonomia  
                 FROM provincias p 
                 GROUP BY p.autonomia
                 HAVING SUM(p.superficie)=(SELECT MIN(c1.suma) 
                 FROM (SELECT p.autonomia, SUM(p.superficie) AS suma 
                 FROM provincias p 
                 WHERE p.autonomia IN(SELECT p.autonomia FROM provincias p GROUP BY p.autonomia HAVING COUNT(*)>4) GROUP BY p.autonomia) AS c1)))) AS c2) ',
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);
        return $this->render("resultados", [
                    "resultados" => $dataProvider,
                    "campos" => ['provincia'],
                    "titulo" => "Consulta 1",
                    "enunciado" => "la consulta mumero 1 del examen",
                    "sql" => "lo primero que hacemos es coger que autonomias tienen mas de 4 provincias. despues cogemos la superficie de estas. 
                      luego cogemos la menos extensa con un min. luego de saber cual es esa autonomia, pasamos a mirar que provincias tiene y de las que tiene cogemos la menos poblada."
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
