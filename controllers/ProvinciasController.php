<?php

namespace app\controllers;

use Yii;
use app\models\Provincias;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProvinciasController implements the CRUD actions for Provincias model.
 */
class ProvinciasController extends Controller
{
    
    public function actionConsulta1() {
        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SELECT p.provincia 
                FROM provincias p 
                WHERE p.poblacion=(SELECT MIN(c2.poblacion) 
                FROM (SELECT p.provincia,p.poblacion FROM provincias p 
                WHERE p.provincia IN (SELECT p.provincia 
                FROM provincias p 
                WHERE p.autonomia=
                (SELECT p.autonomia  
                 FROM provincias p 
                 GROUP BY p.autonomia
                 HAVING SUM(p.superficie)=(SELECT MIN(c1.suma) 
                 FROM (SELECT p.autonomia, SUM(p.superficie) AS suma 
                 FROM provincias p 
                 WHERE p.autonomia IN(SELECT p.autonomia FROM provincias p GROUP BY p.autonomia HAVING COUNT(*)>4) GROUP BY p.autonomia) AS c1)))) AS c2) ',
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);
        return $this->render("resultados", [
                    "resultados" => $dataProvider,
                    "campos" => ['provincia'],
                    "titulo" => "Consulta 1 con DAO",
                    "enunciado" => "Número de ciclistas que hay",
                    "sql" => "SELECT COUNT(*) FROM ciclista c"
        ]);
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Provincias models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Provincias::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Provincias model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Provincias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Provincias();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->provincia]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Provincias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->provincia]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Provincias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Provincias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Provincias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Provincias::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
